package AIF.AerialVehicles;

import AIF.Entities.Coordinates;

public abstract class Hermes extends Uav {
    Hermes(Coordinates currentLocation, int numberOfStations, Class[] compatibleEquipments) {
        super(currentLocation, 10000 , numberOfStations, compatibleEquipments);
    }
}
