package AIF.AerialVehicles;

public abstract class Equipment {
    private boolean used = false;
    private int range;

    Equipment(int range){
        this.range = range;
    }

    public int getRange() {
        return this.range;
    }

    public void active(){
        this.used = true;
    }

    public boolean isUsed() {
        return this.used;
    }
}
