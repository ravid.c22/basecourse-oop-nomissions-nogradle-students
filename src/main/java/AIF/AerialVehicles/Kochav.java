package AIF.AerialVehicles;

import AIF.Entities.Coordinates;
import AIF.AerialVehicles.Weapon;

public class Kochav extends Hermes {
    Kochav(Coordinates currentLocation) {
        super(currentLocation, 5,new Class[] {Weapon.class, BDA.class, Sensor.class});
    }
}
