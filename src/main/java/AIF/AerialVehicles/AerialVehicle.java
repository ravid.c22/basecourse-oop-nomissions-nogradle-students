package AIF.AerialVehicles;


import AIF.Entities.Coordinates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;

public abstract class AerialVehicle {
    boolean isAirborne;
    Coordinates currentLocation;
    int distanceToMaintenance;
    int remainsForMaintenance;
    int numberOfStations;
    Class[] compatibleEquipments;
    ArrayList<Equipment> stations;

    AerialVehicle(Coordinates currentLocation, int distanceToMaintenance, int numberOfStations, Class[] compatibleEquipments) {
        this.isAirborne = false;
        this.currentLocation = currentLocation;
        this.distanceToMaintenance = distanceToMaintenance;
        this.remainsForMaintenance = distanceToMaintenance;
        this.numberOfStations = numberOfStations;
        this.compatibleEquipments = compatibleEquipments;
        this.stations = new ArrayList<>();
    }

    public void takeOff() {
        if (!this.isAirborne) {
            System.out.println("Taking off");
        } else {
            System.out.println("The aircraft isn't ready");
        }
    }

    public void flyTo(Coordinates destination) {
        if (this.isAirborne) {
            System.out.println("Flying to:" + destination);
        } else {
            System.out.println("The aircraft isn't airborne");
        }
    }

    public void land() {
        if (this.isAirborne) {
            System.out.println("Landing");
        } else {
            System.out.println("The aircraft isn't airborne");
        }
    }

    public boolean needMaintenance() {
        if (this.remainsForMaintenance <= 0) {
            return true;
        } else {
            return false;
        }
    }

    public void performMaintenance() {
        if (!this.isAirborne) {
            this.remainsForMaintenance = this.distanceToMaintenance;
            System.out.println("Performing maintenance");
        } else {
            System.out.println("The aircraft isn't airborne");
        }
    }

    public void loadModule(Equipment equipmentToLoad) {
        if(numberOfStations > stations.size()  &&
                Arrays.asList(this.compatibleEquipments).contains(equipmentToLoad.getClass())) {
            this.stations.add(equipmentToLoad);
            System.out.println(equipmentToLoad.getClass().toString() + "was loaded");
        } else {
            System.out.println("the equipment can't load");
        }
    }

    public void activateModule(Class typeOfEquipment, Coordinates target) {
        Optional<Equipment> equipment = this.stations.stream().filter((station) -> typeOfEquipment.equals(station.getClass())).findFirst();

        if (equipment.isEmpty()) {
            System.out.println("Equipment type does not exist");
        } else if (equipment.get().isUsed()) {
            System.out.println("The equipment was used");
        } else if(this.currentLocation.distance(target) > equipment.get().getRange()) {
            System.out.println("The target is too far away");
        } else {
                equipment.get().active();
                System.out.println("You turned on the equipment");
        }
    }
}
