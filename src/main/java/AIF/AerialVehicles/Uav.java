package AIF.AerialVehicles;

import AIF.Entities.Coordinates;

public abstract class Uav extends AerialVehicle {

    Uav(Coordinates currentLocation, int distanceToMaintenance, int numberOfStations, Class[] compatibleEquipments) {
        super(currentLocation, distanceToMaintenance, numberOfStations, compatibleEquipments);
    }

    public void hoverOverLocation(double hours) {
        if (this.isAirborne) {
            this.remainsForMaintenance -= 150 * hours;
            System.out.println("Hovering over:" + this.currentLocation);
        } else {
            System.out.println("The aircraft isn't airborne");
        }
    }
}
