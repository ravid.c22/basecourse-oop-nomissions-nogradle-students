package AIF.AerialVehicles;


import AIF.Entities.Coordinates;

public class F15 extends FighterJet {
    F15(Coordinates currentLocation) {
        super(currentLocation, 10, new Class[] {Weapon.class,Sensor.class});
    }
}
