package AIF.AerialVehicles;


import AIF.Entities.Coordinates;

public class F16 extends FighterJet {

    public F16(Coordinates currentLocation) {
        super(currentLocation, 7, new Class[] {Weapon.class,BDA.class});
    }
}
