package AIF.AerialVehicles;

import AIF.Entities.Coordinates;

public abstract class Haron extends Uav {
    Haron(Coordinates currentLocation, int availableStations, Class[] compatibleEquipments) {
        super(currentLocation, 15000, availableStations, compatibleEquipments);
    }
}
