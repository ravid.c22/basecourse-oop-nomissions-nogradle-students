package AIF.AerialVehicles;

import AIF.Entities.Coordinates;

public abstract class FighterJet extends AerialVehicle {

    FighterJet(Coordinates currentLocation, int numberOfStations, Class[] compatibleEquipments) {
        super(currentLocation, 25000 , numberOfStations, compatibleEquipments);
    }
}
